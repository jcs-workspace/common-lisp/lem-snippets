;; This is how you define command in lem!

(defpackage :my-command
  (:use :cl :lem)
  (:export :my-command))

(in-package :my-command)

(define-command my-command () ()
   (message "lem is awesome"))
